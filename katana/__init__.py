# -*- coding: utf-8 -*-
"""Katana: A python3 Sudoku library."""

__version__ = "2.0.0"  # Package version
__author__ = "Joseph Choufani <joseph.choufani.dev@gmail.com>"
