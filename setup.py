#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from distutils.core import setup
import katana


AUTHOR, AUTHOR_EMAIL = map(
    lambda x: x.strip('< >'), katana.__author__.split(' <')
)


setup(
    name='Katana Sudoku',
    description='Python3 Sudoku utilities library',
    version=katana.__version__,
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    url='https://gitlab.com/joseph.choufani/katana',
    packages=['katana']
)
