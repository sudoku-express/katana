# -*- coding: utf-8 -*-
"""Sudoku utilities library module."""


from __future__ import print_function, division, unicode_literals
import math
import random
from . import gridlib
from .gridlib import DEFAULT_GRID_SIZE
from .sequences import SequenceStop, lrtb_seq_next, slalom_next

__all__ = (
    'SimpleFlag', 'SudokuError', 'SudokuGrid', 'PuzzleCell', 'SudokuPuzzle',
)
__version__ = 9  # Incrementa module version
__author__ = "Joseph Choufani <joseph.choufani.dev@gmail.com>"


try:
    #  Overrides python-2's `range` with `xrange`-behaviour
    range = xrange  # pylint: disable=invalid-name,redefined-builtin
except NameError:
    #  Python-3
    pass


VERSION_CODE = __version__
"""int: Legacy incremental version number."""


#  TODO 2018-04-25 Replace with an enumerator using the `enum` module
SUDOKU_EASY = 1
"""int: Sudoku puzzle ``easy`` level constant."""

SUDOKU_MEDIUM = 2
"""int: Sudoku puzzle ``medium`` level constant."""

SUDOKU_HARD = 3
"""int: Sudoku puzzle ``hard`` level constant."""

SUDOKU_VERY_HARD = 4
"""int: Sudoku puzzle ``very hard`` level constant."""

SUDOKU_IMPOSSIBLE = 5
"""int: Sudoku puzzle ``impossible`` level constant."""


SUDOKU_METRICS = {
    SUDOKU_EASY: (1.6, 1.25, 1.8),
    SUDOKU_MEDIUM: (2.25, 1.6, 2.25),
    SUDOKU_HARD: (2.5, 2.25, 3.0),
    SUDOKU_VERY_HARD: (2.8, 2.5, 4.5),
    SUDOKU_IMPOSSIBLE: (3.6, 2.8, float("infinity")),
}
"""dict: Sudoku metrics mapping.

They are based on found metrics for a 9x9 grid in Table 1 and Table 2 of
section 4 of the `Sudoku Paper`_.

Values are the ratios of:
- the total number of cells in the grid to the upper limit of the range of
  number of givens for the level
- the total number of cells in the grid to the lower limit of the range of
  number of givens for the level
- the grid size to the number of givens in a row/column for the level.

These ratios are then used to interpolate the new grid sizes.

.. _Sudoku Paper:
   http://zhangroup.aporc.org/images/files/Paper_3485.pdf

"""


def sudoku_list(grid_values=None, grid_size=DEFAULT_GRID_SIZE, cell_type=None):
    """Generate a list of Sudoku-compatible number cells.

    Args:
        grid_values(iterable, optional): Initial values.
            Initial values can be :obj:`gridlib.GridCell` instances or a value
            convertible to :obj:`int` to be passed to a
            :obj:`gridlib.GridCell` instance.

        grid_size (:obj:`int`, optional): Grid size.
        cell_type (:obj:`gridlib.GridCell`, optional): Cell type.

    Returns:
        :obj:`list` of :obj:`GridCell`: Created list of grid cells.

    Raises:
        :obj:`TypeError`: Invalid cell type.
        :obj:`ValueError`: Invalid initial values.

    """
    list_size = grid_size ** 2  # List size is the square of the grid size
    cell_cls = gridlib.GridCell if cell_type is None else cell_type
    if not issubclass(cell_cls, gridlib.GridCell):
        raise TypeError("invalid cell type")
    if grid_values is None:
        #  Initialises a list of empty grid cells list if no initial values
        return [cell_cls() for _ in range(list_size)]
    #  Validates list of values
    values = list(grid_values)
    if len(values) != list_size:
        raise ValueError("invalid initial grid values")
    for i, val in enumerate(values):
        if not isinstance(val, cell_cls):
            #  Convert value to a GridCell with integer value
            val = values[i] = cell_cls(int(val))
        #  Validates Sudoku-cell value's range is between 1 and grid size
        if not val.is_set or val < 1 or val > grid_size:
            raise ValueError("invalid initial grid values")
    #  Returns valid list
    return values


class SimpleFlag(object):
    """Simple boolean flag wrapper."""

    def __init__(self):
        """Constructor."""
        self.__flag = False

    def __nonzero__(self):
        """Python-2 non-zero check."""
        return self.__flag

    def __bool__(self):
        """Boolean test check."""
        return self.__flag

    def set(self):
        """Set the flag."""
        self.__flag = True

    def clear(self):
        """Clear the flag."""
        self.__flag = False


class SudokuError(ValueError):
    """Sudoku validation error class."""


class SudokuGrid(gridlib.SquareGrid):
    """Sudoku grid wrapper class."""

    def __init__(self, grid_values,
                 grid_size=DEFAULT_GRID_SIZE, validate=True):
        """Constructor.

        Args:
            grid_values (:obj:`list` of :obj:`int`): Grid contents.
                List of values by serialised by line of the grid.
                The size of the list must be be the square of the grid size.

            grid_size (:obj:`int`, optional): Grid size.
                Must be a valid square of an integer (default: 9).

            validate (:obj:`bool`, optional): Validation flag.
                Validation is enabled by default.

        Raises:
            :obj:`SudokuError`: Grid validation failed.
            :obj:`ValueError`: Invalid grid value.

        """
        #  Calls super class constructor
        super(SudokuGrid, self).__init__(grid_values, grid_size)
        if validate:
            #  Validates values
            for i in range(self.size):
                for j in range(self.size):
                    #  First ensures it's an integer
                    val = self.item(i, j)
                    #  Second makes sure it has a proper value
                    # in range [1...`grid_size`].
                    if val < 1 or val > self.size:
                        raise ValueError("invalid grid value: %s" % val)
                    #  Updates integer value in grid
                    self[i, j] = val
                #  Validates sub-square
                if len(set(self.get_sub(i))) != self.size:
                    raise SudokuError("values conflict in sub-square %s" % i)
                #  Validates row
                if len(set(self.row(i))) != self.size:
                    raise SudokuError("values conflict in row %s" % i)
                #  Validates column
                if len(set(self.column(i))) != self.size:
                    raise SudokuError("values conflict in column %s" % i)

    @classmethod
    def random(cls, size=DEFAULT_GRID_SIZE, cell_type=None):
        """Generate a Sudoku grid with random values.

        Notes:
            - Might be very time-consuming for large grids. Tested for sizes
              9 (default) and 16 which are the most common puzzle sizes and
              received response times respectively <100ms and <10s.
              Could grow exponentially from there.

        Args:
            size (:obj:`int`, optional): Grid size parameter.
                Must be a valid square of an integer (default: 9).

        Returns:
            :obj:`SudokuGrid`: The created instance.

        Raises:
            :obj:`ValueError`: Invalid grid size or input values.
            :obj:`TypeError`: Potentially raised if the given size cannot be
                converted to :obj:`int`.

        """
        #  Initialises grid with empty cells
        grid = gridlib.SquareGrid(
            sudoku_list(grid_size=size, cell_type=cell_type), size
        )
        #  Prepares fill paramters
        max_idx = grid.size - 1
        all_opts = set(range(1, grid.size + 1))  # All values to fill
        completed = SimpleFlag()   # Filling completion flag

        def _fill(off_x, off_y):
            """Fill grid.

            Recursive function to randomly fill Sudoku grid, adapted from
            `StackOverflow answer`_

            To-dos:
                - Consider optimising using method in `Sudoku Paper`_
                  section 5.2.1.

            Args:
                off_x (int): Cell row offset.
                off_y (int): Cell column offset.


            .. _StackOverflow answer:
                http://stackoverflow.com/a/15690431/3522964

            .. _Sudoku Paper:
                http://zhangroup.aporc.org/images/files/Paper_3485.pdf

            """
            cell = grid.item(off_x, off_y)
            while not completed and (len(cell) < grid.size):
                #  Randomly finds a value that has not been tried yet
                val = random.choice(list(all_opts - set(cell.stack)))
                #  Checks validity of value
                if val in grid.get_item_sub(off_x, off_y) \
                   or val in grid.row(off_x) or val in grid.column(off_y):
                    #  Pushes invalid value to cell's stack and tries again
                    cell.push(val)
                    continue
                #  Sets valid value to cell
                cell.value = val
                #  Recursively fills next cell
                try:
                    _fill(*lrtb_seq_next((off_x, off_y), max_idx))
                except SequenceStop:
                    completed.set()
                    break
            #  Resets cell if not completed
            if not completed:
                cell.reset()

        #  Fills grid starting from first cell
        _fill(0, 0)
        #  Returns class instance (no need for validation)
        return cls(grid, grid.size, validate=False)


class PuzzleCell(gridlib.GridCell):
    """Sudoku puzzle cell.

    Sub-class of :obj:`GridCell` adapted for puzzle generation with the
    digging-holes method.

    """

    def __init__(self, value=None):
        """Constructor.

        Args:
            value (:obj:`int`, optional): Cell initial value.

        """
        super(PuzzleCell, self).__init__(value)
        self.__dig = True
        self.__forbid = False

    @property
    def is_diggable(self):
        """:obj:`bool`: Read-only flag indicating if the cell is diggable."""
        return self.__dig

    @property
    def is_forbidden(self):
        """:obj:`bool`: Read-only flag indicating if the cell is forbidden."""
        return self.__forbid

    def dig(self):
        """Set current cell as digged.

        Unsets current value.

        """
        self.__dig = False
        self.value = None

    def forbid(self):
        """Set current cell as forbidden.

        Resets `diggable` flag.

        """
        self.__forbid = True
        self.__dig = False

    def reset(self):
        """Reset cell.

        Overrides reset method to account for resetting flags.

        """
        super(PuzzleCell, self).reset()
        #  Diggable by default
        self.__dig = True
        #  Unforbidden by default
        self.__forbid = False


def get_metrics(difficulty, grid_size=DEFAULT_GRID_SIZE):
    """Get Sudoku generation metrics based on difficulty and size.

    The returned metrics are the range of number of givens in the grid
    and the minimum number of givens in a row/column.

    Values are based on found metrics in Table 1 and Table 2 of section 4 of
    the `Sudoku Paper`_.

    In the paper, the metrics are given for a 9x9 grid. In order to generalise
    these values based on the grid size, we find the ratios of the total
    number of cells in the grid (81) to respectively the upper and lower
    limits of the givens range, and the ratio of the size (9) to the minimum
    number of givens in row/column. These ratios are then used to interpolate
    the new grid sizes.

    Args:
        difficulty (int): Difficulty level from the `SUDOKU_*` constants.
        grid_size (:obj:`int`, optional): Grid size.

    Returns:
        :obj:`tuple` of :obj:`range` and :obj:`int`:
            The 2-tuple conatining the range of number of givens in the grid
            and the minimum number of givens in a row/column.

    Raises:
        :obj:`ValueError`: Invalid difficulty level is provided.

    .. _Sudoku Paper:
        http://zhangroup.aporc.org/images/files/Paper_3485.pdf

    """
    try:
        high, low, ratio = SUDOKU_METRICS[difficulty]
    except KeyError:
        raise ValueError("invalid grid difficulty %s" % repr(difficulty))
    #  Interpolates difficulty metrics based on grid size
    return (
        #  Range of given values
        range(int(grid_size ** 2 // high), int(grid_size ** 2 // low)),
        #  Minimum number of givens
        int(math.ceil(grid_size / ratio))
    )


class UserControlException(Exception):
    """Exception used to stop a thread's calculations on user control."""


def check_step_control(event):
    """Check control event status.

    Suitable in a multi-threaded environment running parallel tasks
    (parallel solving tasks for example). Checks if the control event is set
    and stops current thread's calculations by raising a
    :obj:`UserControlException`.

    Args:
        event (:obj:`threading.Event`): Control event.

    Raises:
        :obj:`UserControlException`:
            Control event is set and execution must be stopped.

    """
    if event is not None and event.is_set():
        raise UserControlException


def rand_decision():
    """Make a random decision.

    Returns:
        bool: Randomly-selected ``True`` or ``False`` decision.

    """
    return random.choice((True, False))


def rand_distinct_values(src_vals, count=2):
    """Randomly select multiple distinct values.

    Args:
        src_val (iterable): Source iterable of values.
        count (:obj:`int`, optional): Number of values to generate.

    """
    #  Loading all values is faster for random selection
    all_vals = list(src_vals)
    while all_vals and count > 0:
        #  Selects a random value
        val = random.choice(all_vals)
        yield val
        #  Removes selection for next iteration
        all_vals.remove(val)
        #  Decrements count for next iteration
        count = count - 1


def get_level_sequencer(level, force_random=False,
                        grid_size=DEFAULT_GRID_SIZE):
    """Get level sequencer.

    Args:
        level (int): Grid difficulty level.
            A valid value from `SUDOKU_*` constants
        force_random (:obj:`bool`, optional): Random sequence forcing flag.
        grid_size (:obj:`int`, optional): Grid size.

    Returns:
        lambda: Lambda function with 1 :obj:`tuple` parameter that determines
                the next item in the appropriate level sequence and returns
                its index 2-tuple.

    """
    if force_random or level in (SUDOKU_EASY, SUDOKU_MEDIUM):
        #  Randomising globally in easy and medium modes (or forced random)
        return \
            lambda _: (
                random.choice(range(grid_size)),
                random.choice(range(grid_size))
            )
    if level == SUDOKU_HARD:
        #  Jumping one cell in hard mode
        return \
            lambda idx: \
            (0, 0) if not idx or None in idx \
            else slalom_next(idx, grid_size - 1, 2)
    if level == SUDOKU_VERY_HARD:
        #  Wandering along S in very hard mode
        return \
            lambda idx: \
            (0, 0) if not idx or None in idx \
            else slalom_next(idx, grid_size - 1)
    if level == SUDOKU_IMPOSSIBLE:
        #  LRTB sequence in "impossible" mode
        return \
            lambda idx: \
            (0, 0) if not idx or None in idx \
            else lrtb_seq_next(idx, grid_size - 1)
    #  Raises an error for invalid level
    raise ValueError(level)


class SudokuPuzzle(gridlib.SquareGrid):
    """Sudoku puzzle wrapper class."""

    def __init__(self, grid_values, grid_size=DEFAULT_GRID_SIZE):
        """Construtor.

        Args:
            grid_values (:obj:`list` of :obj:`int`): Grid contents.
                List of values by serialised by line of the grid.
                The size of the list must be be the square of the grid size.

            grid_size (:obj:`int`): Grid size.
                Must be a valid square of an integer (default: 9).

        Raises:
            :obj:`ValueError`: Invalid grid size or input values.

        """
        super(SudokuPuzzle, self).__init__((
            c if isinstance(c, gridlib.GridCell) else gridlib.GridCell(c)
            for c in grid_values
        ), grid_size)

    def propagate_swap_values(self, val1, val2):
        """Generate grid by swap-propagating two values in the grid.

        As per the `Sudoku Paper`_ figure 5.5, swapping values in a Sudoku
        grid generates a new sudoku grid.

        Args:
            val1 (int): First value.
            val2 (int): Second value.

        Returns:
            :obj:`SudokuPuzzle`: Generated sudoku puzzle with swapped values.

        Raises:
            :obj:`ValueError`: An invalid value was provided.

        .. _Sudoku Paper:
            http://zhangroup.aporc.org/images/files/Paper_3485.pdf

        """
        #  Validates values
        if val1 == val2 or \
           val1 < 1 or val1 > self.size or val2 < 1 or val2 > self.size:
            raise ValueError("invalid value")

        def _generator(grid):
            """Generate a list of grid elements with propagated swapped values.

            Args:
                grid (:obj:`SudokuPuzzle`): Grid to propagate value swapping.

            Yields:
                int: Grid element or swapped value

            """
            for cell in grid:
                try:
                    # Gets the cell value
                    val = cell.value
                except AttributeError:
                    val = None
                #  Yields appropriate value
                yield val2 if val == val1 else val1 if val == val2 else val

        #  The generator function is used instead of a simple generator
        # expression iterating over the grid elements because the function
        # makes comparisons using the raw grid cell value thus removing some
        # performance overhead for checking for custom comparison functions.
        return SudokuPuzzle(_generator(self), self.size)

    def swap_columns(self, col1, col2):
        """Generate grid by swapping two columns in current grid.

        Args:
            col1 (int): Index of the first column.
            col2 (int): Index of the second column.

        Returns:
            :obj:`SudokuPuzzle`: Generated sudoku puzzle with swapped columns.

        """
        #  Creates a copy of the current puzzle
        tmp = SudokuPuzzle(self)
        #  Swaps column values using the magical powers of Python!!1
        for i in range(tmp.size):
            tmp[i, col1], tmp[i, col2] = tmp[i, col2], tmp[i, col1]
        #  Returns swapped version
        return tmp

    def propagate_columns_swap(self, col1, col2):
        """Generate new puzzle grid using two columns swap propagation.

        As per the `Sudoku Paper`_ figure 5.5, swapping columns in a Sudoku
        grid in the same block area generates a new sudoku grid.

        Args:
            col1 (int): Index of the first column.
            col2 (int): Index of the second column.

        Returns:
            :obj:`SudokuPuzzle`: Generated sudoku puzzle with swapped columns.

        Raises:
            :obj:`ValueError`: The provided values are not in the same block
                area or provided columns are identical.

        .. _Sudoku Paper:
            http://zhangroup.aporc.org/images/files/Paper_3485.pdf

        """
        #  Validates columns indices
        block_size = int(math.sqrt(self.size))
        if col1 == col2 or col1 // block_size != col2 // block_size:
            raise ValueError
        return self.swap_columns(col1, col2)

    def propagate_blocks_swap(self, block1, block2):
        """Generate new puzzle using two column block areas swap propagation.

        As per the `Sudoku Paper`_ figure 5.5, swapping columns block areas in
        a Sudoku grid linearly generates a new sudoku grid.

        Args:
            block1 (int): The index of the first block area.
            block2 (int): The index of the second block area.

        Returns:
            :obj:`SudokuPuzzle`: Generated sudoku puzzle with swapped block
                areas.

        Raises:
            :obj:`ValueError`: Invalid index values.

        .. _Sudoku Paper:
            http://zhangroup.aporc.org/images/files/Paper_3485.pdf

        """
        #  Validates blocks indices
        block_size = int(math.sqrt(self.size))
        if block1 == block2 \
           or block1 >= block_size or block2 >= block_size:
            raise ValueError
        #  Creates a copy of current cell
        tmp = SudokuPuzzle(self)
        #  Swaps columns linearly in blocks areas
        for i in range(block_size):
            tmp = tmp.swap_columns(
                block1 * block_size + i, block2 * block_size + i
            )
        #  Returns swapped version
        return tmp

    def propagate_grid_roll(self):
        """Generate a grid using a grid roll propagation.

        Rolling is the process of rotating cell values by 90 degrees.

        As per the `Sudoku Paper`_ figure 5.5, rolling a Sudoku grid generates
        a new sudoku grid.

        Returns:
            :obj:`SudokuPuzzle`: Generated rolled sudoku puzzle.

        .. _Sudoku Paper:
            http://zhangroup.aporc.org/images/files/Paper_3485.pdf

        """
        #  Generates a new 90-degree-rolled puzzle
        return SudokuPuzzle((
            self.item(self.size - j - 1, i).value
            for i in range(self.size) for j in range(self.size)
        ), self.size)

    def random_propagation(self, max_depth=4, **force_method):
        """Generate a new Sudoku puzzle based on current puzzle.

        Applies random propagation operations on current puzzle.

        Args:
            max_depth (:obj:`int`, optional): Maximum number of propgations.
            force_method: Keyword-arguments list to force propagation methods.
                The following values are supported:

                - swap (bool): Force values swap.
                - col_swap (bool): Force columns swap.
                - block_swap (bool): Force blocks areas swap.
                - grid_roll (bool): Force grid rolling.

        Returns:
            :obj:`SudokuPuzzle`: Generated random-propagated sudoku puzzle.

        """
        #  There are at most 4 supported propagations
        props = min(max_depth, 4)
        #   Creates a copy of the current grid
        tmp = SudokuPuzzle(self)
        #  Randomly decides to swap values:
        if force_method.get('swap') or props > 0 and rand_decision():
            #  Randomly selects 2 distinct values to swap
            val1, val2 = rand_distinct_values(range(1, self.size + 1))
            tmp = tmp.propagate_swap_values(val1, val2)
            #  Decrements remaining propagations
            props -= 1
        #  Randomly decides to swap columns in a block area
        block_size = int(math.sqrt(self.size))
        if force_method.get('col_swap') or props > 0 and rand_decision():
            #  Randomly selects a block area
            block = random.choice(range(block_size))
            #  Randomly selects two distinct offset indices to swap
            val1, val2 = rand_distinct_values(range(block_size))
            tmp = tmp.propagate_columns_swap(
                block * block_size + val1, block * block_size + val2
            )
            #  Decrements remaining propagations
            props -= 1
        #  Randomly decides to swap block area
        if force_method.get('block_swap') \
           or props > 0 and random.choice((True, False)):
            #  Randomly selects two distinct block areas to swap
            val1, val2 = rand_distinct_values(range(block_size))
            tmp = tmp.propagate_blocks_swap(val1, val2)
            #  Decrements remaining propagations
            props -= 1
        #  Randomly decides to roll grid
        if force_method.get('grid_roll') or props > 0 and rand_decision():
            tmp = tmp.propagate_grid_roll()
        return tmp

    def solve(self, control=None):
        """Solve current puzzle.

        Args:
            control (:obj:`threading.Event`, optional): Completion event.
                Used for multi-threaded solving control to signal the
                successful completion in another thread.

        Returns:
            :obj:`SudokuGrid`: Validated solution instance.

        Raises:
            :obj:`SudokuError`: Failure to solve puzzle.
            :obj:`UserControlException`:
                Control event is set and execution must be stopped.

        """
        check_step_control(control)  # Checks completion at every step
        #  Copies the puzzle into a grid of puzzle cells
        grid = gridlib.SquareGrid((
            PuzzleCell(c.value if isinstance(c, gridlib.GridCell) else c)
            for c in self
        ), self.size)
        check_step_control(control)  # Checks completion at every step
        #  Forbids set givens cells
        for i in range(grid.size):
            check_step_control(control)  # Checks completion at every step
            for j in range(grid.size):
                check_step_control(control)  # Checks completion at every step
                if grid.item(i, j).is_set:
                    grid.item(i, j).forbid()
        # Solution and backtrack flags
        solved, backtrack = SimpleFlag(), SimpleFlag()
        #  Solver parameters
        max_idx, all_opts = grid.size - 1, set(range(1, grid.size + 1))

        def _solver(off_x, off_y):
            """Solver function.

            Recursive function to solve Sudoku grid inspired from
            `StackOverflow answer for Sudoku board generation`_.

            Args:
                off_x (int): Cell row index offset.
                off_y (int): Cell column index offset.

            Raises:
                :obj:`UserControlException`:
                    Control event is set and execution must be stopped.

            .. _StackOverflow answer for Sudoku board generation:
                http://stackoverflow.com/a/15690431/3522964

            """
            cell = grid.item(off_x, off_y)
            while not solved and len(cell) < grid.size:
                check_step_control(control)  # Checks completion at every step
                if not cell.is_forbidden:
                    #  Randomly finds a value that has not been tried yet
                    val = random.choice(list(all_opts - set(cell.stack)))
                    #  Checks validity of value
                    if val in grid.get_item_sub(off_x, off_y) \
                       or val in grid.row(off_x) or val in grid.column(off_y):
                        #  Pushes invalid value to cell's stack and tries again
                        cell.push(val)
                        continue
                    #  Sets valid value to cell
                    cell.value = val
                elif backtrack:
                    break
                try:
                    #  Clears backtracking flaging
                    backtrack.clear()
                    #  Recursively fills next cell in sequence
                    _solver(*lrtb_seq_next((off_x, off_y), max_idx))
                except SequenceStop:
                    #  Marks solved at the end of the sequence
                    solved.set()
                    break
            #  Resets non-forbidden cell if solving not complete
            if not cell.is_forbidden and not solved:
                cell.reset()
                backtrack.set()
            check_step_control(control)  # Checks completion at every step

        #  Starts solving at first cell
        _solver(0, 0)
        #  Result
        if not solved:
            raise SudokuError
        return SudokuGrid(iter(grid), grid.size)

    @classmethod
    def random(cls, grid_difficulty=SUDOKU_EASY, grid_size=DEFAULT_GRID_SIZE,
               force_random=False, control=None):
        """Randomly generate a Sudoku puzzle.

        Based on algortihm in `Sudoku Paper`_ section 5.2.2.

        Args:
            grid_difficulty (:obj:`int`, optional): Grid difficulty.
                A valid value from `SUDOKU_*` constants
                (default: `SUDOKU_EASY`).

            grid_size (:obj:`int`, optional): Grid size.
            force_random (:obj:`bool`, optional): Force random sequence.
            control (:obj:`threading.Event`, optional): Completion event.
                Used for multi-threaded solving control to signal the
                successful completion in another thread.

        Returns:
            :obj:`SudokuPuzzle`: Created puzzle instance.

        Raises:
            :obj:`UserControlException`:
                Control event is set and execution must be stopped.

        .. _Sudoku Paper:
            http://zhangroup.aporc.org/images/files/Paper_3485.pdf

        """
        #  Generates a random Sudoku puzzle grid
        grid = gridlib.SquareGrid(
            SudokuGrid.random(grid_size, PuzzleCell), grid_size
        )
        #  Level-dependent metrics
        givens_range, min_bound = get_metrics(grid_difficulty, grid_size)
        #  Level-dependent sequencer
        next_index = \
            get_level_sequencer(grid_difficulty, force_random, grid_size)
        #  Finds maximum number of givens based on level range
        max_givens = random.choice(givens_range)
        #  Iterates until there are no more diggable cells

        def given_num(itr):
            """Count current number of diggable or forbidden cells."""
            #  Slightly slower than len(list(...)) but more memory-efficient
            return sum((1 for c in itr if c.is_diggable or c.is_forbidden), 0)

        cell_idx, total_givens = (None,) * 2, grid.size ** 2

        def _grid_diggable(puzzle):
            """Check if a grid contains any diggable cells.

            Args:
                puzzle (:obj:`gridlib.SquareGrid`): Grid to check.

            Returns:
                bool: The presence of a diggable cell.

            Raises:
                :obj:`UserControlException`:
                    Control event is set and execution must be stopped.

            """
            for cell in puzzle:
                check_step_control(control)  # Checks completion at every step
                if cell.is_diggable:
                    #  Returns at the first diggable cell
                    return True
            #  All cells are not diggable
            return False

        while _grid_diggable(grid):  # This could be a redundant check
            try:
                #  Finds the next diggable cell in sequence
                while None in cell_idx or not grid.item(*cell_idx).is_diggable:
                    cell_idx = next_index(cell_idx)
                    #  Checks completion at every step
                    check_step_control(control)
            except SequenceStop:
                #  No diggable cells found
                break
            #  Checks for constraints. Constraints checks are not grouped to
            # keep the row/column givens values to be used in case of success.
            # 1. Total givens constraint
            if total_givens == max_givens:
                grid.item(*cell_idx).forbid()
                continue
            check_step_control(control)  # Checks completion at every step
            # 2. Row givens constraint
            row_givens = given_num(grid.row(cell_idx[0])) - 1
            if row_givens < min_bound:
                grid.item(*cell_idx).forbid()
                continue
            check_step_control(control)  # Checks completion at every step
            # 3. Column givens constraint
            col_givens = given_num(grid.column(cell_idx[1])) - 1
            if col_givens < min_bound:
                grid.item(*cell_idx).forbid()
                continue
            check_step_control(control)  # Checks completion at every step
            #  Checks if the solution is unique when current cell is digged
            if row_givens == grid_size - 1 \
               or col_givens == grid_size - 1 \
               or SudokuPuzzle.has_unique_solution(grid, cell_idx, control):
                grid.item(*cell_idx).dig()
                total_givens = total_givens - 1
            else:
                check_step_control(control)  # Checks completion at every step
                #  Otherwise sets cell as forbidden
                grid.item(*cell_idx).forbid()
        #  Returns a SudokuPuzzle instance of the created puzzle
        return cls(grid, grid_size)

    @staticmethod
    def has_unique_solution(puzzle_grid, hole_idx, control=None):
        """Check a grid has a unique solution.

        Variates value at a hole and tries to solve. A successful solution
        means that the grid has multiple solutions.

        Args:
            puzzle_grid(:obj:`SudokuPuzzle`): Puzzle grid.
            hole_idx(:obj:`tuple`): Index of hole cell to check.
            control (:obj:`threading.Event`, optional): Completion event.
                Used for multi-threaded solving control to signal the
                successful completion in another thread.

        Returns:
            bool: True if grid has unique solution. False otherwise.

        To-dos:
            - Use randomisation and GridCell stack to exhaust candidates

        """
        #  Copies grid to an new `SudokuPuzzle` instance
        grid = SudokuPuzzle(puzzle_grid, puzzle_grid.size)
        check_step_control(control)  # Checks completion at every step
        cell = grid.item(*hole_idx)
        #  Gets the list of candidate values for the cell
        candidates = set(range(1, grid.size + 1)) \
            - set(c.value for c in grid.get_item_sub(*hole_idx)) \
            - set(c.value for c in grid.row(hole_idx[0])) \
            - set(c.value for c in grid.column(hole_idx[1]))
        #  Unsets cell value
        cell.unset()
        #  Tries to solve by fixing a random candidate
        while len(cell) <= len(candidates):
            check_step_control(control)  # Checks completion at every step
            cell.value = random.choice(tuple(candidates - set(cell.stack)))
            try:
                #  If the grid was solved for other value, the puzzle doesn't
                # have a unique solution.
                grid.solve(control)
                return False
            except (SudokuError, UserControlException):
                pass
        #  A puzzle has a unique solution if all candidates are exhausted.
        return True
